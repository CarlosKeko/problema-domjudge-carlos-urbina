import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
	
class DOMJudgeProblemaTestSumarPunts {
	
	@Test
	void testSumaSacrifici() {
		int res = DOMJudgeProblema.sumarPunts("sacrifici", 0);
		assertEquals(2000, res, "Resultat incorrecte");
	}
	
	@Test
	void testSumaMort() {
		int res = DOMJudgeProblema.sumarPunts("mort", 0);
		assertEquals(0, res, "Resultat incorrecte");
	}
	
	@Test
	void testSumaBonaAccioGran() {
		int res = DOMJudgeProblema.sumarPunts("bona acció gran", 0);
		assertEquals(250, res, "Resultat incorrecte");
	}
	
	@Test
	void testSumaBonaAccioNormal() {
		int res = DOMJudgeProblema.sumarPunts("bona acció normal", 0);
		assertEquals(150, res, "Resultat incorrecte");
	}
	
	@Test
	void testSumaBonaAccioPetita() {
		int res = DOMJudgeProblema.sumarPunts("bona acció petita", 0);
		assertEquals(50, res, "Resultat incorrecte");
	}
	
	@Test
	void testSumaAssassinat() {
		int res = DOMJudgeProblema.sumarPunts("assassinat", 0);
		assertEquals(-4000, res, "Resultat incorrecte");
	}
	
	@Test
	void testSumaAccioDolentaGran() {
		int res = DOMJudgeProblema.sumarPunts("acció dolenta gran", 0);
		assertEquals(-350, res, "Resultat incorrecte");
	}
	
	@Test
	void testSumaAccioDolentaNormal() {
		int res = DOMJudgeProblema.sumarPunts("acció dolenta normal", 0);
		assertEquals(-250, res, "Resultat incorrecte");
	}
	
	@Test
	void testSumaAccioDolentaPetita() {
		int res = DOMJudgeProblema.sumarPunts("acció dolenta petita", 0);
		assertEquals(-150, res, "Resultat incorrecte");
	}
	
}
