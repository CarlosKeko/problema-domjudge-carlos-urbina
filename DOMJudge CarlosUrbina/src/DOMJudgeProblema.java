import java.util.Scanner;

/**
 * Problema de DOMJudge
 * Aquest codi executarà el sistema de puntuació del més allà.
 * @author Carlos Urbina
 * @version 1.0
 */
public class DOMJudgeProblema {

	public static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			int casos = sc.nextInt();
			
			while (casos > 0) {
				comptarPunts();
				casos--;
				
			}
			
		}catch (Exception e) {
			System.out.println("Valor incorrecte");
			
		}
	}
	/**
	 * La funció demanarà el nom del cas, a continuació farà un bucle i demanarà a l'usuari introduir un valor String, 
	 * després trucarà a una altra funció per calcular l'acció, el bucle finalitzarà quan el valor introduït sigui "mort" o "sacrifici", 
	 * després de sortir del bucle trucarà a una altra funció per donar la resposta final.
	 * @see sumarPunts
	 * @see eleccio
	 */
	public static void comptarPunts() {
		String accio = "";
		int puntuacio = 0;
		
		String nom = sc.next();

		while (!accio.equals("mort") && !accio.equals("sacrifici")) {
			accio = sc.nextLine().toLowerCase().trim();
			puntuacio = sumarPunts(accio, puntuacio);
			
		}

		eleccio(puntuacio, nom);
	}
	
	/**
	 * Aquesta funció serveix per sumar els punts de la variable puntuació que rep, té dos vectors que es relacionen entre si, després fa un bucle amb
	 * la longitud d'una de les dues (les dues tenen la mateixa longitud), i va comparant amb l'acció que hem introduït i la funció ha rebut, si l'accio
	 * és igual al vector de String, suma a la puntuació els punts de l'altre vector que estigui en la posició corresponent.
	 * @param accio rep un valor String
	 * @param puntuacio rep un valor int
	 * @return puntuacio retorna un valor enter
	 */
	public static int sumarPunts(String accio, int puntuacio) {
		String[] accions = {"sacrifici", "mort", "bona acció gran", "bona acció normal", "bona acció petita", "assassinat", "acció dolenta gran", "acció dolenta normal", "acció dolenta petita"};
		int[] puntsAccions = {2000, 0, 250, 150, 50, -4000, -350, -250, -150};
		
		for (int i = 0; i < accions.length; i++) {
			if (accio.equals(accions[i])) {
				puntuacio = puntuacio + puntsAccions[i];
				
			}
		}
		
		return puntuacio;
	}
	
	/**
	 * Aquesta funció rep la puntuació i veu si és major o igual de 2500, si és així respon que el seu lloc és el lloc bo, si és al contrari, respon que
	 * el seu lloc és el lloc dolent.
	 * @param puntuacio rep un valor enter
	 * @param nom rep un valor String
	 */
	public static void eleccio(int puntuacio, String nom) {
		if (puntuacio >= 2500) {
			System.out.println("La puntuació de " + nom + " és de " + puntuacio + " punts, el seu lloc és el lloc bo.");
			
		}else {
			System.out.println("La puntuació de " + nom + " és de " + puntuacio + " punts, el seu lloc és el lloc dolent.");
			
		}
		
	}

}
